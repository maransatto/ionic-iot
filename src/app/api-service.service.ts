import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';

@Injectable()
export class ApiServiceService {

  public API_URL = 'http://demo.thingsboard.io';

  constructor(public httpClient: HttpClient) { }

  getHeader() {
    const usuario = JSON.parse(localStorage.getItem('usuario-api'));
    const headers = {
      headers: {
        'X-Authorization': 'Bearer ' + (usuario ? usuario.token : '')
      }
    };
    // console.log(headers);
    return headers;
  }

  postLogin(username: string, password: string) {
    return this.httpClient.post(`${this.API_URL}/api/auth/login`, {
      "username" : username,
      "password": password
    });
  }

  getESP32Data() {
    const entityType = 'DEVICE';
    const entityId = '68603e80-7d7e-11e9-a0d4-777cc84329e8';
    const startTs = new Date(new Date().setHours(new Date().getHours() - 8, 0, 0, 0)).getTime();
    const endTs = new Date().getTime();

    console.log(startTs);
    console.log(endTs);

    return this.httpClient.get(
      `${this.API_URL}/api/plugins/telemetry/${entityType}/${entityId}/values/timeseries?keys=temperatura,umidade&startTs=${startTs}&endTs=${endTs}&interval=60000&limit=1000&agg=AVG`,
      this.getHeader());
  }
}
