import { EChartOption } from 'echarts';
import { ApiServiceService } from './../api-service.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-umidade',
  templateUrl: './umidade.page.html',
  styleUrls: ['./umidade.page.scss'],
})
export class UmidadePage implements OnInit, OnDestroy {

  public items: Array<{ data: number; valor: number; }> = [];

  intervalHandler;
  chartOptions: EChartOption = {};

  constructor(
    private apiService: ApiServiceService
  ) {
  }

  ngOnInit() {
    this.buscarDadosDaApi();
    // setInterval(() => {
    //   this.buscarDadosDaApi();
    // }, 2000);
  }

  buscarDadosDaApi() {
    this.apiService.getESP32Data().subscribe((result) => {
      this.items = [];
      if (result) {
        result['umidade'].forEach(t => {
          this.items.push({
            data: t.ts,
            valor: t.value
          });
        });
        this.defineChart();
      }
    }, err => {
      console.log(err);
    });
  }

  defineChart() {
    const xAxisData = [];
    const data1 = [];

    for (let i = 0; i < this.items.length; i++) {
      var date = new Date(this.items[i].data);
      var hours = date.getHours();
      var minutes = "0" + date.getMinutes();
      var formattedTime = hours + ':' + minutes.substr(-2);
      xAxisData.push(formattedTime);
      data1.push(parseFloat(this.items[i].valor.toString()).toFixed(2));
    }

    this.chartOptions = {
      legend: {
        data: ['Temperatura'],
        align: 'left'
      },
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false
        },
        boundaryGap: false,
      },
      yAxis: {
      },
      series: [{
        name: 'temperatura',
        type: 'line',
        data: data1,
        animationDelay(idx) {
          return idx * 10;
        },
        label: {
          normal: {
            show: true,
            position: 'top'
          }
        },
        itemStyle: {
          color: 'blue'
        }
      }],
      animationEasing: 'elasticOut',
      animationDelayUpdate(idx) {
        return idx * 5;
      }
    };
  }

  ngOnDestroy() {
    clearInterval(this.intervalHandler);
  }

}
