import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UmidadePage } from './umidade.page';

describe('UmidadePage', () => {
  let component: UmidadePage;
  let fixture: ComponentFixture<UmidadePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UmidadePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UmidadePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
