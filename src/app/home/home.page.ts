import { EChartOption } from 'echarts';
import { ApiServiceService } from './../api-service.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  chartOptions: EChartOption = {};
  dados;

  constructor(
    private apiService: ApiServiceService
  ) {
    localStorage.removeItem('usuario_api');
    this.apiService.postLogin('christophertsen@hotmail.com', 'xis0102').subscribe((result) => {
      localStorage.setItem('usuario-api', JSON.stringify({
        token: result['token']
      }));
      this.buscaDadosDaApi();
    });
  }

  buscaDadosDaApi() {
    this.apiService.getESP32Data().subscribe((result) => {
      if (result) {
        this.dados = result;
        console.log(this.dados);
        this.defineChart();
      }
    }, err => {
      console.log(err);
    });
  }

  defineChart() {
    const xAxisData = [];
    const data1 = [];
    const data2 = [];

    for (let i = 0; i < this.dados.temperatura.length; i++) {
      const temperatura = this.dados.temperatura[i];
      const umidade = this.dados.umidade[i];
      var date = new Date(temperatura.ts);
      var hours = date.getHours();
      var minutes = "0" + date.getMinutes();
      var formattedTime = hours + ':' + minutes.substr(-2);
      xAxisData.push(formattedTime);
      data2.push(parseFloat(umidade.value.toString()).toFixed(2));
      data1.push(parseFloat(temperatura.value.toString()).toFixed(2));
    }

    this.chartOptions = {
      legend: {
        data: ['Temperatura'],
        align: 'left'
      },
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false
        },
        boundaryGap: false,
      },
      yAxis: {
      },
      series: [{
        name: 'temperatura',
        type: 'line',
        data: data1,
        animationDelay(idx) {
          return idx * 10;
        },
        label: {
          normal: {
            show: true,
            position: 'top'
          }
        }
      },{
        name: 'umidade',
        type: 'line',
        data: data2,
        animationDelay(idx) {
          return idx * 10;
        },
        label: {
          normal: {
            show: true,
            position: 'top'
          }
        },
        itemStyle: {
          color: 'blue'
        }
      }],
      animationEasing: 'elasticOut',
      animationDelayUpdate(idx) {
        return idx * 5;
      }
    };
  }

}
